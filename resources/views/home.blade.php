<x-layout>
    <style>
        .smashery-container {
            width: 100%;
            max-width: 600px;
            height: 100px;
            margin: 0 auto;
        }

        #smashery-text {
            fill: #c2185b;
            font-family: 'BERNIER Distressed', sans-serif;
            font-size: 95px;
            font-weight: bold;
        }

        #smashery-line {
            fill: none;
            stroke-width: 5;
            stroke-linecap: round;
            stroke-linejoin: round;
            stroke-dasharray: 2000;
            stroke-dashoffset: 2000;
            animation: draw 10s linear infinite;
        }

        @keyframes draw {
            0% {
                stroke-dashoffset: 2000;
                stroke: #c2185b;
            }

            50% {
                stroke-dashoffset: 0;
                stroke: #FF6B00;
            }

            100% {
                stroke-dashoffset: -2000;
                stroke: #c2185b;
            }
        }

        .backpink {
            background-color: #c2185b;
            /* Colore di sfondo principale - Magenta */
            background-image:
                radial-gradient(#e91e63 1px, transparent 1px),
                /* Pallini - Rosa leggermente più chiaro */
                radial-gradient(#e91e63 1px, transparent 1px);
            background-size: 10px 10px;
            /* Distanza tra i pallini */
            background-position: 0 0, 5px 5px;
        }

        .backorange {
            background-image: url("{{ asset('storage/hollowed-boxes.svg') }}");

        }

        .st0 {
            fill: #FF6B00;
        }

        #wave {
            width: 100% !important;
        }

        .smashitwithshadow {
            color: #FF6400;
            font-family: "Bernier", Sans-serif;
            font-size: 11vw;
            font-weight: 600;
            text-shadow: -21px 34px 0px rgba(255, 99.99999999999999, 0, 0.16);
        }
    </style>
    <main>
        <div class="container-fluid backpink">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-xs-12 col-md-4 col-lg-4 col-xl-4 text-center mt-5">
                    <svg height="400px" width="400px" version="1.1" id="_x32_" xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512" xml:space="preserve">

                        <g>

                            <path class="st0" d="M303.319,176.627c7.615,0,13.783-6.185,13.783-13.779c0-7.627-6.168-13.796-13.783-13.796
                    c-7.61,0-13.779,6.169-13.779,13.796C289.541,170.442,295.709,176.627,303.319,176.627z" />
                            <path class="st0" d="M397.963,176.627c7.614,0,13.782-6.185,13.782-13.779c0-7.627-6.168-13.796-13.782-13.796
                    c-7.611,0-13.788,6.169-13.788,13.796C384.175,170.442,390.352,176.627,397.963,176.627z" />
                            <path class="st0" d="M114.046,176.627c7.615,0,13.783-6.185,13.783-13.779c0-7.627-6.168-13.796-13.783-13.796
                    c-7.611,0-13.784,6.169-13.784,13.796C100.262,170.442,106.435,176.627,114.046,176.627z" />
                            <path class="st0" d="M208.685,176.627c7.614,0,13.783-6.185,13.783-13.779c0-7.627-6.169-13.796-13.783-13.796
                    s-13.787,6.169-13.787,13.796C194.897,170.442,201.07,176.627,208.685,176.627z" />
                            <path class="st0" d="M256.002,103.635c7.618,0,13.787-6.168,13.787-13.771c0-7.627-6.169-13.787-13.787-13.787
                    c-7.61,0-13.779,6.16-13.779,13.787C242.223,97.467,248.392,103.635,256.002,103.635z" />
                            <path class="st0" d="M348.769,105.257c7.61,0,13.783-6.168,13.783-13.779c0-7.618-6.173-13.787-13.783-13.787
                    c-7.615,0-13.787,6.169-13.787,13.787C334.983,99.089,341.155,105.257,348.769,105.257z" />
                            <path class="st0" d="M163.247,105.257c7.611,0,13.783-6.168,13.783-13.779c0-7.618-6.172-13.787-13.783-13.787
                    c-7.614,0-13.787,6.169-13.787,13.787C149.46,99.089,155.633,105.257,163.247,105.257z" />


                            <path class="st0" d="M396.886,273.417c5.148-2.843,10.604-4.776,19.96-4.834c8.31,0.041,13.545,1.606,18.218,3.908
                    c3.498,1.753,6.722,4.072,10.257,6.939c5.292,4.236,11.178,9.839,19.521,14.606c8.319,4.776,19.109,8.16,32.36,8.102
                    c6.951,0,12.583-5.636,12.583-12.583c0-6.955-5.632-12.583-12.583-12.583c-8.312-0.041-13.546-1.614-18.22-3.908
                    c-3.498-1.753-6.722-4.08-10.256-6.939c-4.334-3.465-9.146-7.84-15.303-11.928h5.468c17.948,0,33.706-9.478,43.229-25.994
                    c14.574-25.264,12.862-64.848-4.071-94.119C454.468,58.792,364.043,13.842,256.158,13.842
                    c-107.26,0-195.324,43.656-241.614,119.777c-17.35,28.558-19.414,67.986-4.915,93.766c9.585,17.04,25.547,26.813,43.795,26.813
                    h5.214c-2.818,1.876-5.424,3.793-7.738,5.661c-6.189,5.054-10.989,9.413-16.118,12.272c-5.148,2.851-10.613,4.776-19.976,4.842
                    c-6.951,0-12.583,5.628-12.583,12.583c0,6.947,5.632,12.583,12.583,12.583c11.784,0.041,21.622-2.588,29.511-6.578
                    c5.924-2.966,10.704-6.537,14.828-9.872c6.189-5.054,10.986-9.404,16.114-12.272c5.153-2.843,10.617-4.776,19.981-4.834
                    c8.322,0.041,13.565,1.606,18.248,3.908c3.502,1.753,6.733,4.08,10.269,6.939c4.514,3.605,9.556,8.184,16.081,12.411H66.388
                    c-22.54,0-40.804,18.268-40.804,40.805c0,13.263,6.418,24.92,16.208,32.375c-7.595,1.892-17.171,5.874-25.142,14.598
                    c-6.725,7.365-14.348,20.538-12.452,41.452c4.252,46.834,33.383,77.088,74.216,77.088h177.592h177.592
                    c40.834,0,69.961-30.253,74.212-77.079c1.901-20.923-5.722-34.096-12.448-41.46c-7.971-8.724-17.548-12.706-25.142-14.598
                    c9.79-7.455,16.208-19.112,16.208-32.375c0-22.536-18.264-40.805-40.805-40.805h-73.393c3.129-2.024,6.029-4.112,8.552-6.152
                    C386.969,280.634,391.765,276.284,396.886,273.417z M482.747,418.801c-2.404,26.493-16.167,54.191-49.149,54.191H256.006H78.414
                    c-32.978,0-46.748-27.698-49.152-54.191c-2.748-30.319,24.826-30.319,24.826-30.319h403.837
                    C457.925,388.483,485.499,388.483,482.747,418.801z M36.045,146.693C82.617,70.113,168.527,39.008,256.158,39.008
                    c87.627,0,176.781,32.826,220.108,107.685c19.224,33.219,11.817,82.339-17.374,82.339H53.424
                    C24.231,229.032,16.134,179.461,36.045,146.693z M219.998,285.688c6.185-5.054,10.982-9.404,16.102-12.272
                    c5.149-2.843,10.605-4.776,19.96-4.834c8.319,0.041,13.558,1.606,18.236,3.908c3.502,1.753,6.73,4.08,10.265,6.939
                    c4.51,3.605,9.552,8.184,16.073,12.411h-89.187C214.575,289.817,217.475,287.728,219.998,285.688z M219.478,254.198
                    c-2.818,1.876-5.423,3.793-7.738,5.669c-6.185,5.055-10.982,9.405-16.102,12.272c-5.149,2.842-10.604,4.768-19.96,4.833
                    c-8.323-0.041-13.566-1.614-18.248-3.916c-3.502-1.745-6.734-4.072-10.269-6.939c-4.342-3.465-9.158-7.832-15.315-11.92H219.478z
                    M356.425,272.139c-5.15,2.842-10.604,4.768-19.96,4.833c-8.319-0.041-13.558-1.614-18.236-3.908
                    c-3.502-1.753-6.73-4.08-10.264-6.939c-4.338-3.465-9.155-7.84-15.311-11.928h87.61c-2.818,1.876-5.423,3.793-7.738,5.669
                    C366.341,264.921,361.545,269.271,356.425,272.139z" />

                        </g>
                        <text id="smashery-text" x="258" y="365" text-anchor="middle">SMASHERY</text>
                    </svg>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-12 text-center">
                    <h2 class="font-bernier" style="font-size: 4rem">MENU'</h2>
                    <p class="text-light fs-3">Scopri tutti i sapori dei nostri deliziosi Smash Burger!</p>
                    <p class="text-light fs-3">E per concludere in dolcezza, assapora i nostri irresistibili dolci!</p>
                    {{-- <img src="{{ asset('storage/Smashery-V2.png') }}" alt="Hamburger"
                        class="hero-image img-fluid w-25"><br> --}}
                    <a href="#" class="cta-button">MENU HAMBURGER</a>
                    <a href="#" class="cta-button">MENU SWEETS</a>
                </div>
                
            </div>
            <div class="row justify-content-center">
                <div class="col-12 m-0 p-0" style="margin-top:-4vw!important">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                        <path fill="#ff6b00" fill-opacity="1"
                            d="M0,0L34.3,10.7C68.6,21,137,43,206,69.3C274.3,96,343,128,411,160C480,192,549,224,617,240C685.7,256,754,256,823,256C891.4,256,960,256,1029,250.7C1097.1,245,1166,235,1234,197.3C1302.9,160,1371,96,1406,64L1440,32L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z">
                        </path>
                    </svg>
                </div>
            </div>
        </div>
        <div class="container-fluid backorange align-content-center">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-xs-12 col-md-4 col-lg-4 col-xl-4 text-start align-content-center">
                    <img src="{{ asset('storage/AVATAR-TIPO-HAPPY.png') }}" alt="" class="img-fluid p-3">

                </div>
                <div class="col-12 col-sm-12 col-xs-12 col-md-8 col-lg-8 col-xl-8 text-end p-5 align-content-center">
                    <div class="container-fluid" style="background-color: #c2185b;border-radius:40px">
                        <div class="row justify-content-end">
                            <div class="col-12 text-end p-3 me-0">
                                <h2 class="test-text">SMASH E' MEGLIO!</h2>
                                <p class="text-light text-center fs-3">Scopri Smashery, la nuova hamburgeria di Catania che porta
                                    l'innovativa tecnica di preparazione
                                    degli
                                    "smash burger". La nostra carne macinata viene pressata su una piastra bollente,
                                    creando una
                                    crosta
                                    croccante e mantenendo i succhi all'interno per un hamburger succulento.
                                    Un'esperienza gustativa
                                    unica
                                    con pane fresco e ingredienti di alta qualità.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid backpink">
            <div class="row justify-content-center">
                <div class="col-12 m-0 p-0">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                        <g transform="scale(1, -1) translate(0, -320)">
                            <path fill="#ff6b00" fill-opacity="1"
                                d="M0,0L34.3,10.7C68.6,21,137,43,206,69.3C274.3,96,343,128,411,160C480,192,549,224,617,240C685.7,256,754,256,823,256C891.4,256,960,256,1029,250.7C1097.1,245,1166,235,1234,197.3C1302.9,160,1371,96,1406,64L1440,32L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z">
                            </path>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <h2 class="smashitwithshadow font-bernier" style="margin-top:-14vw;margin-left:19vw">SMASH IT!</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                
                <div class="col-12 col-sm-12 col-xs-12 col-md-4 col-lg-4 col-xl-4">
                    <img src="{{ asset('storage/BURGER-MENU-PIC-1.png') }}" alt="" class="img-fluid p-3">
                </div>
                <div class="col-12 col-sm-12 col-xs-12 col-md-8 col-lg-8 col-xl-8 text-center" style="margin-top:11vw">
                    <p class="text-light fs-3">Gli hamburger arrivano al centro di Catania! Smashery è un concetto
                        innovativo che trasforma
                        l'esperienza
                        di gustare un hamburger in qualcosa di straordinario.</p>
                    <a href="#" class="cta-button" style="background-color: #E5007D; color: white;">MENU
                        HAMBURGER</a>
                </div>
            </div>
        </div>
        <div class="container-fluid backpink">
            <div class="row justifu-content-center">
                <div class="col-12 text-center">
                    <h2 class="text-light font-bernier smashitwithshadow">SMASH IT!</h2>
                </div>
                
                <div class="col-12 col-sm-12 col-xs-12 col-md-8 col-lg-8 col-xl-8 text-center"  style="margin-top:11vw">
                    <p class="text-light fs-3">Deliziosi dolci ti aspettano, impreziositi da creme e altre golosità!</p>
                    <p class="text-light fs-3">Per un gusto... esplosivo!</p>
                    <a href="#" class="cta-button">MENU SWEETS</a>
                </div>
                <div class="col-12 col-sm-12 col-xs-12 col-md-4 col-lg-4 col-xl-4">
                    <img src="{{ asset('storage/Smashery-V2-20.png') }}" alt="" class="img-fluid p-3">
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row justify-content-between">
                <div class="col-12 col-sm-12 col-xs-12 col-md-4 col-lg-4 col-xl-4">
                    <img src="{{ asset('storage/pic-1.png') }}" class="img-fluid" alt="">
                </div>
                <div class="col-12 col-sm-12 col-xs-12 col-md-8 col-lg-8 col-xl-8 text-center align-content-center">
                    <h2 class="test-text">I NOSTRI PRODOTTI</h2>
                    <div class="lista backpink text-center">
                        <p class="text-light">Da Smashery troverai solo il meglio per un'esperienza gustativa unica:</p>
                        <ul
                            style="text-align: left; display: flex; flex-wrap: wrap; padding: 0; list-style: none; width: auto; justify-content: space-between;">
                            <li class="text-light"
                                style="display: block; margin-bottom: 10px; position: relative; width: calc(50% - 10px);">
                                Carne di altissima qualità</li>
                            <li class="text-light"
                                style="display: block; margin-bottom: 10px; position: relative; width: calc(50% - 10px);">
                                Snack internazionali</li>
                            <li class="text-light"
                                style="display: block; margin-bottom: 10px; position: relative; width: calc(50% - 10px);">
                                Pane realizzato ad hoc</li>
                            <li class="text-light"
                                style="display: block; margin-bottom: 10px; position: relative; width: calc(50% - 10px);">
                                Ortaggi freschissimi</li>
                            <li class="text-light"
                                style="display: block; margin-bottom: 10px; position: relative; width: calc(50% - 10px);">
                                Bevande e birre</li>
                            <li class="text-light"
                                style="display: block; margin-bottom: 10px; position: relative; width: calc(50% - 10px);">
                                Dessert del giorno</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
</x-layout>
