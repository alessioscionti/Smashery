<header>
  <div class="logo"><img src="{{asset("storage/MARCHIO.PNG")}}" alt=""></div>
  <nav>
    <div class="container-fluid">
      <row class="justify-content-center">
        <div class="col-12 text-center font-bernier">
          
          <a href="#menu">Menu</a>
          <a href="#chi-siamo">Chi siamo</a>
          <a href="#prodotti">Prodotti</a>
        </div>
      </row>
    </div>
  </nav>
</header>