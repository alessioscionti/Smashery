<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/js/app.js', 'resources/css/app.css'])
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Smashery</title>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
</head>
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #FF6B00;
    }
    header {
        background-color: #FF6B00;
        padding: 10px 0;
        text-align: right;
    }
    nav a {
        color: white;
        text-decoration: none;
        margin-left: 20px;
    }
    .logo {
        float: left;
        width: 50px;
        height: 50px;
        background-color: white;
        border-radius: 50%;
        margin-left: 20px;
    }
    .hero {
        background-color: #E5007D;
        color: white;
        padding: 50px 20px;
        text-align: center;
    }
    .hero h1 {
        margin-top: 16rem;
        font-size: 4em;
        margin-bottom: 20px;
    }
    .hero-image {
        max-width: 100%;
        height: auto;
    }
    .cta-button {
        display: inline-block;
        background-color: white;
        color: #E5007D;
        padding: 10px 20px;
        text-decoration: none;
        border-radius: 25px;
        margin: 10px;
    }
    .section {
        padding: 50px 20px;
        text-align: center;
    }
    .section h2 {
        font-size: 3em;
        color: white;
    }
    .section p {
        color: white;
        max-width: 800px;
        margin: 0 auto;
    }
    .product-image {
        max-width: 100%;
        height: auto;
    }
    footer {
        background-color: #E5007D;
        color: white;
        padding: 20px;
        text-align: center;
    }
</style>
<body>
    <x-navbar/>
    {{$slot}}
    <x-footer/>
</body>
</html>