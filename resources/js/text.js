const letters = document.querySelectorAll('.smashery-text span');
const colors = ['goldenrod']; 

letters.forEach(letter => {
  const randomColor = colors[Math.floor(Math.random() * colors.length)];
  letter.style.setProperty('--random-color', randomColor);
});