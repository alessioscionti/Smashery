import './bootstrap';
import './mininav';
import './navbar';
import './text';
import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
